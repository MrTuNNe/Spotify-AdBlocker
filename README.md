<h1 align="center">
  Spotify AdBlocker for Linux
</h1>

<b>This script must be running as root OR with "sudo" command.</b>

## How to use?
```
# First you need to download this script. Using this command
$ git clone https://github.com/MrTuNNe/Spotify-AdBlocker.git
# Then go into the folder
$ cd Spotify-AdBlocker
# And the last step
$ sudo python spotify-adblock.py
```
## Which linux distro should I use?
I tested this script on Linux Mint, but will also work without errors on Ubuntu, Debian, Fedora.


That's all. Simple, fast and efficient.
